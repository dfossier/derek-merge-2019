﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicTracker : MonoBehaviour
{
    public string RepresentationId;
    // Start is called before the first frame update
    private void Start()
    {
        MiniMap.Current.SubscribeTrackedMinimapObject(RepresentationId, transform.position, this.gameObject);
    }

    private void OnDestroy()
    {
        MiniMap.Current.DisposeTrackedMinimapObject(this.gameObject);
    }
}
