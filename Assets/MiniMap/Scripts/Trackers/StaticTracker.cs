﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticTracker : MonoBehaviour
{
    public string RepresentationId;
    // Start is called before the first frame update
    private void Start()
    {
        MiniMap.Current.SubscribeStaticMinimapObject(RepresentationId, transform.position, this.gameObject);
    }

    private void OnDestroy()
    {
        MiniMap.Current.DisposeStaticMinimapObject(this.gameObject);
    }
}
