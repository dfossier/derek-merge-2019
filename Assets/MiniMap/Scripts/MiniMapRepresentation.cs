﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MiniMapRepresentation
{
    public GameObject RepresentationGameObject;
    public string Id;
}
