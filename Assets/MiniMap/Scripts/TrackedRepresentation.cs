﻿using UnityEngine;

[System.Serializable]
public class TrackedRepresentation
{
    public GameObject TrackedObject;
    public RectTransform Representation;

    public TrackedRepresentation (GameObject _trackedObject, RectTransform _rectTransform)
    {
        TrackedObject = _trackedObject;
        Representation = _rectTransform;
    }
}
