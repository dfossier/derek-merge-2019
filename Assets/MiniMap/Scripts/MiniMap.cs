﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
    public static MiniMap Current;
    public float MapSize;
    public Vector2 MinimapResolution;
    public Vector3 MapCenter;
    public float CameraHeight = 20;
    private Vector2 minimapSize;
    public LayerMask TerrainLayer;
    private RectTransform minimapRectTransform;
    public RawImage MapDisplay;
    private Camera camera;
    private RenderTexture renderTexture;
    public MiniMapRepresentation DefaultRepresentation;
    public List<MiniMapRepresentation> MinimapRepresentations;
    private List<TrackedRepresentation> TrackedRepresentations;
    private List<TrackedRepresentation> StaticRepresentations;

    // Start is called before the first frame update
    void Awake()
    {
        if(Current == null)
        {
            Current = this;
        }
        TrackedRepresentations = new List<TrackedRepresentation>();
        StaticRepresentations = new List<TrackedRepresentation>();
        var screenShotCamera = new GameObject();
        screenShotCamera.transform.rotation = Quaternion.Euler(new Vector3(90,0,0));
        screenShotCamera.transform.position = new Vector3(MapCenter.x, CameraHeight, MapCenter.z);
        camera = screenShotCamera.AddComponent(typeof(Camera)) as Camera;
        camera.orthographic = true;
        camera.orthographicSize = MapSize * 0.5f;
        renderTexture = new RenderTexture((int)MinimapResolution.x, (int)MinimapResolution.y, 16, RenderTextureFormat.ARGB64);
        camera.targetTexture = renderTexture;
        camera.clearFlags = CameraClearFlags.Nothing;
        camera.cullingMask = TerrainLayer;
        MapDisplay.texture = renderTexture;
        StartCoroutine(DisableScreenShotCamOnNextFrame());
        minimapRectTransform = GetComponent<RectTransform>();
        minimapSize = new Vector2(minimapRectTransform.sizeDelta.x, minimapRectTransform.sizeDelta.y);
    }

    private void Update()
    {
        minimapSize = new Vector2(minimapRectTransform.sizeDelta.x, minimapRectTransform.sizeDelta.y);
        foreach (var trackedRepresentation in TrackedRepresentations)
        {
            trackedRepresentation.Representation.localPosition = new Vector3(linear(trackedRepresentation.TrackedObject.transform.position.x - MapCenter.x, -MapSize, MapSize, -minimapSize.x, minimapSize.x), linear(trackedRepresentation.TrackedObject.transform.position.z - MapCenter.z, -MapSize, MapSize, -minimapSize.y, minimapSize.y), 0);
        }
    }

    public void SubscribeTrackedMinimapObject(string representationId, Vector3 position, GameObject gameObject)
    {
        foreach (var minimapRepresentation in MinimapRepresentations)
        {
            if (representationId == minimapRepresentation.Id)
            {
                AddRepresentation(TrackedRepresentations, position, gameObject, minimapRepresentation);
                return;
            }
        }
        AddRepresentation(TrackedRepresentations, position, gameObject, DefaultRepresentation);
    }

    private void AddRepresentation(List<TrackedRepresentation> list, Vector3 position, GameObject gameObject, MiniMapRepresentation representation)
    {
        var Representation = Instantiate(representation.RepresentationGameObject, MapDisplay.transform);
        var rectTransform = Representation.GetComponent<RectTransform>();
        rectTransform.localPosition = new Vector3(linear(position.x - MapCenter.x, -MapSize, MapSize, -minimapSize.x, minimapSize.x), linear(position.z - MapCenter.z, -MapSize, MapSize, -minimapSize.y, minimapSize.y), 0);
        var staticObject = new TrackedRepresentation(gameObject, rectTransform);
        list.Add(staticObject);
    }

    public void DisposeTrackedMinimapObject(GameObject gameObject)
    {
        foreach (var trackedRepresentation in TrackedRepresentations)
        {
            if (trackedRepresentation.TrackedObject == gameObject)
            {
                Destroy(trackedRepresentation.Representation.gameObject);
                StaticRepresentations.Remove(trackedRepresentation);
                return;
            }
        }
    }

    public void SubscribeStaticMinimapObject(string representationId, Vector3 position, GameObject gameObject)
    {
        foreach(var minimapRepresentation in MinimapRepresentations)
        {
            if(representationId == minimapRepresentation.Id)
            {
                AddRepresentation(StaticRepresentations, position, gameObject, minimapRepresentation);
                return;
            }
        }

        AddRepresentation(StaticRepresentations, position, gameObject, DefaultRepresentation);
    }

    public void DisposeStaticMinimapObject(GameObject gameObject)
    {
        foreach(var staticRepresentation in StaticRepresentations)
        {
            if(staticRepresentation.TrackedObject == gameObject)
            {
                Destroy(staticRepresentation.Representation.gameObject);
                StaticRepresentations.Remove(staticRepresentation);
                return;
            }
        }
    }

    IEnumerator DisableScreenShotCamOnNextFrame()
    {
        yield return new WaitForEndOfFrame();
        camera.gameObject.SetActive(false);
    }

    static public float linear(float x, float x0, float x1, float y0, float y1)
    {
        if ((x1 - x0) == 0)
        {
            return (y0 + y1) / 2;
        }
        return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
    }
}
