﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGeneration : MonoBehaviour
{
    //[SerializeField]
    //private Object trackingSphere;

    [SerializeField]
    public GameObject[] treeList;

    public int treeCount = 0;

    [SerializeField]
    private int minorTreeRadius;

    private int biomeIndex = 1;

    [SerializeField]
    private GameObject[] treePrefab;

    [SerializeField]
    private float waterHeight = 2f;

    [SerializeField]
    private float treeWater = 0.25f;

    [SerializeField]
    private float treeHeat = .2f;

    private float neighborRadius;

    public void GenerateTrees(int tilesWidth, int verticesWidth, TerrainData terrainData)
    {
        for (int xIndex = 4; xIndex < (tilesWidth * (verticesWidth)-1); xIndex++)
        {
            for (int zIndex = 4; zIndex < (tilesWidth * (verticesWidth)-1); zIndex++)
            {
                //if (treeCount > 5000) { break; }
                
                // convert from Level Coordinate System to Tile Coordinate System and retrieve the corresponding TileData
                TileCoordinate tileCoordinate = terrainData.ConvertToTileCoordinate(xIndex, zIndex);
                HeightMap treeHeightMap = terrainData.chunksData[tileCoordinate.tileXIndex, tileCoordinate.tileZIndex];

                neighborRadius = minorTreeRadius;
                float maxValue = 0f;
                float treeValue = treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)];


                if (treeValue >= .85)
                {
                    neighborRadius = 0;
                }


                // compares the current tree noise value to the neighbor ones
                // this is where you calculate tree density... highest noisemap value compared to neighbors of a certain radius
                // the problem is that you only look for neighbors once then run through all of the trees... need this to be linked together somehow
                int neighborXBegin = (int)Mathf.Max(0, tileCoordinate.coordinateXIndex - neighborRadius);
                int neighborXEnd = (int)Mathf.Min(verticesWidth - 1, tileCoordinate.coordinateXIndex + neighborRadius);
                int neighborZBegin = (int)Mathf.Max(0, tileCoordinate.coordinateZIndex - neighborRadius);
                int neighborZEnd = (int)Mathf.Min(verticesWidth - 1, tileCoordinate.coordinateZIndex + neighborRadius);

                for (int neighborZ = neighborZBegin; neighborZ <= neighborZEnd; neighborZ++)
                {
                    for (int neighborX = neighborXBegin; neighborX <= neighborXEnd; neighborX++)
                    {
                        float neighborValue = treeHeightMap.moisture[(verticesWidth - 1 - neighborX), (verticesWidth - 1 - neighborZ)];

                        // save the maximum tree noise value in the radius
                        if (neighborValue - .05 >= maxValue)
                        {
                            maxValue = neighborValue;
                        }
                        //Debug.Log("neighborValue = " + neighborValue);
                    }
                }

                //if the current tree noise value is the maximum one, place a tree in this location
                if (treeValue >= maxValue)
                {
                    //the trees are drawn with a normal coordinate system with zero at bottom left, but the terrain data somehow is zero from bottom right of bottom left tile
                    //therefor what we need to do is to reassign the height values to pull from the bottom left.

                    if (treeHeightMap.heightvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= waterHeight
                    //&& treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= treeWater
                    && treeHeightMap.heat[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] <= treeHeat)
                    {
                        // 0,0 is the center of the first tile... the build up and to the right... so we have to move origin by half the width of each tile
                        // minus tilesWidth is to adjust for overlapping vertices
                        Vector3 treePosition = new Vector3(xIndex - ((verticesWidth + 5) / 2),
                            treeHeightMap.heightvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex),
                            (verticesWidth - 1 - tileCoordinate.coordinateZIndex)],

                            //this is where the z goes... difficult.  each tile from top to bottom but tiles are from bottom up.  So we first add height for the tile (0,1,2) then subtrack it based on zindex)
                            //then add back double the amount of vertices for which the zindex of the tile is below origin (to mirror it up)

                            ((-zIndex + (verticesWidth - 1) / 2) + (2*tileCoordinate.tileZIndex) * (verticesWidth-tileCoordinate.tileZIndex-1) ));

                        if (treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= .5)
                        {
                            biomeIndex = 0;
                        }
                        else
                        {
                            biomeIndex = 1;
                        }

                        GameObject tree = Instantiate(this.treePrefab[biomeIndex], treePosition, Quaternion.identity) as GameObject;
                        tree.transform.parent = treeList[biomeIndex].transform;

                        //Debug.Log("verticesWidth " + (verticesWidth) + " zIndex " + zIndex + " tileCoordinate.tileZIndex" + tileCoordinate.tileZIndex + " Tree Z value: " + ((-zIndex - (verticesWidth - tilesWidth) / 2) - (1 * (tileCoordinate.tileZIndex) * verticesWidth)));
                        //Debug.Log("X: " + xIndex + " Z: " + zIndex + " water: " + treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)]);

                        //use this to make object bigger or smaller
                        //tree.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                        treeCount++;
                    }
                }
            }
        }
    }
}
