﻿using RTSEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FreeUnitGeneration : MonoBehaviour
{
    //[SerializeField]
    //private Object trackingSphere;

    [SerializeField]
    public GameObject unitList;

    public int unitCount = 0;

    [SerializeField]
    private int minorUnitRadius;

    [SerializeField]
    private int uBiomeIndex = 1;

    [SerializeField]
    private GameObject[] unitPrefab;

    [SerializeField]
    private float unitHeight = 2f;

    [SerializeField]
    private float unitWater = 0.25f;

    [SerializeField]
    private float unitHeat = .2f;

    private int unitAttempts;

    private float uNeighborRadius;
    private (int, int, int) unitPosition;

    [SerializeField]
    private UnitManager unitMgr;

    //get game manager to register units
    private void Awake()
    {
        //old code not sure if we will use
        //targetAmount = amountRange.getRandomValue(); //set the target amount of units to spawn
        //spawnReload = spawnReloadRange.getRandomValue();

        if (unitMgr == null) //if the Unit Manager component hasn't been set, get it
            unitMgr = FindObjectOfType(typeof(UnitManager)) as UnitManager;
    }


    public void GenerateUnits(int tilesWidth, int verticesWidth, TerrainData terrainData)
    {
        for (int xIndex = 4; xIndex < (tilesWidth * (verticesWidth) - 1); xIndex++)
        {
            for (int zIndex = 4; zIndex < (tilesWidth * (verticesWidth) - 1); zIndex++)
            {
                if (unitCount > 50) { break;}

                // convert from Level Coordinate System to Tile Coordinate System and retrieve the corresponding TileData
                TileCoordinate tileCoordinate = terrainData.ConvertToTileCoordinate(xIndex, zIndex);
                HeightMap unitHeightMap = terrainData.chunksData[tileCoordinate.tileXIndex, tileCoordinate.tileZIndex];

                uNeighborRadius = minorUnitRadius;
                float maxUnitValue = 0f;
                float unitValue = unitHeightMap.unitvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)];
                //Debug.Log("xIndex: tilecoordinate.coordinateXIndex" + xIndex + "+" + tileCoordinate.coordinateXIndex + "vert width" + verticesWidth);
                //Debug.Log("zIndex: tilecoordinate.coordinateZIndex" + zIndex + "+" + tileCoordinate.coordinateZIndex);

                //if (unitValue >= .95)
                //{
                //    uNeighborRadius = 0;
                //}


                //// compares the current tree noise value to the neighbor ones
                //// this is where you calculate tree density... highest noisemap value compared to neighbors of a certain radius
                //// the problem is that you only look for neighbors once then run through all of the trees... need this to be linked together somehow
                //int neighborXBegin = (int)Mathf.Max(0, tileCoordinate.coordinateXIndex - uNeighborRadius);
                //int neighborXEnd = (int)Mathf.Min(verticesWidth - 1, tileCoordinate.coordinateXIndex + uNeighborRadius);
                //int neighborZBegin = (int)Mathf.Max(0, tileCoordinate.coordinateZIndex - uNeighborRadius);
                //int neighborZEnd = (int)Mathf.Min(verticesWidth - 1, tileCoordinate.coordinateZIndex + uNeighborRadius);

                //for (int neighborZ = neighborZBegin; neighborZ <= neighborZEnd; neighborZ++)
                //{
                //    for (int neighborX = neighborXBegin; neighborX <= neighborXEnd; neighborX++)
                //    {
                //        float uNeighborValue = unitHeightMap.moisture[(verticesWidth - 1 - neighborX), (verticesWidth - 1 - neighborZ)];

                //        // save the maximum tree noise value in the radius
                //        if (uNeighborValue - .05 >= maxUnitValue)
                //        {
                //            maxUnitValue = uNeighborValue;
                //        }
                //        //Debug.Log("neighborValue = " + neighborValue);
                //    }
                //}

                //if the current tree noise value is the maximum one, place a tree in this location
                //if (unitValue >= maxUnitValue)
                //{
                    //the trees are drawn with a normal coordinate system with zero at bottom left, but the terrain data somehow is zero from bottom right of bottom left tile
                    //therefor what we need to do is to reassign the height values to pull from the bottom left.

                    if (unitHeightMap.heightvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= unitHeight&&
                    unitHeightMap.unitvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= .98)
                    {
                        // 0,0 is the center of the first tile... the build up and to the right... so we have to move origin by half the width of each tile
                        // minus tilesWidth is to adjust for overlapping vertices
                        Vector3 unitPosition = new Vector3(xIndex - ((verticesWidth + 5) / 2),
                            unitHeightMap.heightvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)],

                            //this is where the z goes... difficult.  each tile from top to bottom but tiles are from bottom up.  So we first add height for the tile (0,1,2) then subtrack it based on zindex)
                            //then add back double the amount of vertices for which the zindex of the tile is below origin (to mirror it up)

                            ((-zIndex + (verticesWidth - 1) / 2) + (2 * tileCoordinate.tileZIndex) * (verticesWidth - tileCoordinate.tileZIndex - 1)));

                    //this code places the unit on the navmesh
                    while (unitAttempts <= 5)
                    {
                        NavMeshHit hit;

                        if (NavMesh.SamplePosition(unitPosition, out hit, 1.0f, NavMesh.AllAreas))
                        {
                            unitPosition = hit.position;
                            break;
                        }
                        else
                        {
                            float newX = unitPosition.x + Random.value * 1;
                            float newZ = unitPosition.z + Random.value * 1;
                            Vector3 newPosition = new Vector3(newX, 1.5f, newZ);
                            unitPosition = newPosition;
                            unitAttempts++;
                        }
                    }
                    unitAttempts = 0;

                        //if (unitHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= .75)
                        //{
                        //    uBiomeIndex = 0;
                        //}
                        //else
                        //{
                        //    uBiomeIndex = 0;
                        //}

                        GameObject unitGameObject = Instantiate(this.unitPrefab[uBiomeIndex], unitPosition, Quaternion.identity) as GameObject;
                        var unit = unitGameObject.GetComponent<Unit>();
                        unitGameObject.transform.parent = unitList.transform;
                        unitMgr.freeUnits.Add(unit);
                        unitCount++;

                    //Debug.Log("verticesWidth " + (verticesWidth) + " zIndex " + zIndex + " tileCoordinate.tileZIndex" + tileCoordinate.tileZIndex + " Tree Z value: " + ((-zIndex - (verticesWidth - tilesWidth) / 2) - (1 * (tileCoordinate.tileZIndex) * verticesWidth)));
                    //Debug.Log("X: " + xIndex + " Z: " + zIndex + " water: " + treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)]);

                    //use this to make object bigger or smaller
                    //tree.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

                }
                //}
            }
        }
    }
}
