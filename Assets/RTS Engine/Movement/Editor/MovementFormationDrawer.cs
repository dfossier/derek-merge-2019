﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

/* MovementFormationDrawer script created by Oussama Bouanani, SoumiDelRio.
 * This script is part of the Unity RTS Engine */

namespace RTSEngine.CustomDrawer
{
    [CustomPropertyDrawer(typeof(MovementFormation))]
    public class MovementFormationDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);

            int heightMultiplier = GetHeightMultiplier(property);
            float height = (position.height - ((heightMultiplier - 1) * EditorGUIUtility.standardVerticalSpacing)) / heightMultiplier;

            Rect formationRect = new Rect(position.x, position.y, position.width, height);
            EditorGUI.PropertyField(formationRect, property.FindPropertyRelative("type"), new GUIContent("Movement Formation"));

            if(property.FindPropertyRelative("type").enumValueIndex == (int)MovementFormation.Type.row)
            {
                bool showProperties = property.FindPropertyRelative("showProperties").boolValue;

                Rect propertiesRect = new Rect(position.x, position.y + height + EditorGUIUtility.standardVerticalSpacing, position.width, height);
                showProperties = EditorGUI.Foldout(propertiesRect, showProperties, new GUIContent("Formation Properties"));

                if (showProperties)
                {
                    EditorGUI.indentLevel++;

                    Rect amountRect = new Rect(position.x, position.y + (height + EditorGUIUtility.standardVerticalSpacing) * 2, position.width, height);
                    EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("amount"), new GUIContent("Amount Per Row"));

                    Rect maxEmptyRect = new Rect(position.x, position.y + (height + EditorGUIUtility.standardVerticalSpacing) * 3, position.width, height);
                    EditorGUI.PropertyField(maxEmptyRect, property.FindPropertyRelative("maxEmpty"), new GUIContent("Max Empty Rows"));

                    EditorGUI.indentLevel--;
                }

                property.FindPropertyRelative("showProperties").boolValue = showProperties;
            }

            EditorGUI.EndProperty();
        }

        //override this function to add space below the new property drawer
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return GetHeightMultiplier(property) * (base.GetPropertyHeight(property, label) + (EditorGUIUtility.standardVerticalSpacing - 1));
        }

        private int GetHeightMultiplier(SerializedProperty property)
        {
            int heightMultiplier = 1;
            bool haveProperties = false;
            switch((MovementFormation.Type)property.FindPropertyRelative("type").enumValueIndex)
            {
                case MovementFormation.Type.row:
                    heightMultiplier++;
                    haveProperties = true;
                    break;
            }

            return heightMultiplier + (haveProperties && property.FindPropertyRelative("showProperties").boolValue ? 2 : 0);
        }
    }
}
